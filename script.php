<?php
//require_once "ePNDump/ePNDump.php";
require "autoload.php";
require 'AliParser/AliParserEn.php';

const DIP_LINK = 'o32nmncbvj7poh6jvu8aroqvl76z9acq';
const PRODUCTS_CHUNK_SIZE = 10;

ini_set('memory_limit', '-1');

$mongoDb = \DB\MongoDB::init();
$mongoDb = $mongoDb->selectDatabase("aliexpress");

function parseDumpFile($mongoDb)
{
    $offerCollection = $mongoDb->selectCollection('offers');
    $ePN = new ePNDump('/media/data/door/ePN/ePNDump/alidump.yml', DIP_LINK);

    MongoHelper::batchInsert($offerCollection, $ePN->getOffers(), 1000);
}

function parseAliProducts($mongoDb)
{
    /** @var \MongoDB\Collection $offerCollection */
    $offerCollection = $mongoDb->selectCollection('offers');
    $cursor = $offerCollection->find();

    $offersIds = $offers = $notFoundOfferIds = [];
    $k = 0;

    foreach ($cursor as $doc) {
        $offersIds[] = $doc['id'];
        if($k > 100) break;
        echo "$k \n";
        $k++;
    }

    $parser = new AliParserEn();
    $offerIdsChunks = array_chunk($offersIds, PRODUCTS_CHUNK_SIZE);

    foreach ($offerIdsChunks as $partIds) {
        $products = $parser->parse($partIds);
//        sleep(2);
        $offers = array_merge($products, $offers);
    }

    foreach ($offers as $offer) {
        $set = [ '$set' => ['fullInfo' => $offer->toArray()]];

        $result = $offerCollection->updateMany(['id' => "{$offer->getId()}"], $set);
    }

    if(count($parser->notFoundIds) > 0) {
        $query = ['id' => ['$in' => $parser->notFoundIds]];
        $set = ['$set' => ['available' => false]];

        $offerCollection->updateMany($query, $set);
    }


    var_dump(implode(',', $parser->notFoundIds));

}

function exitTimeOut($seconds) {
    sleep($seconds);
    exit(0);
}

exitTimeOut(30);
parseAliProducts($mongoDb);

