<?php

class ePNDump
{
    const DATE_ELEMENT_NAME = 'yml_catalog';
    const COMPANY_ELEMENT_NAME = 'company';
    const URL_ELEMENT_NAME = 'url';
    const CURRENCY_ELEMENT_NAME = 'currency';
    const CATEGORY_ELEMENT_NAME = 'category';
    const OFFER_ELEMENT_NAME = 'offer';
    const PICTURE_ELEMENT_NAME = 'picture';

    public
        $dipLink,
        $date,
        $categories,
        $offers = [],
        $currency,
        $url,
        $company;

    protected $xmlReader;

    public function __construct($filePath, $dipLink)
    {
        $this->dipLink = $dipLink;

        $this->fileContent = file_get_contents($filePath);

        $this->xmlReader = new XMLReader();
        $this->xmlReader->open($filePath);

        $this->parse();
    }

    protected function parse()
    {
        while ($this->xmlReader->read()) {
            if ($this->xmlReader->nodeType == XMLReader::ELEMENT) {
                if ($this->xmlReader->localName == self::OFFER_ELEMENT_NAME)
                {
                    $offer = [];

                    $dom = $this->xmlReader->expand();

                    $offer['id'] = $this->xmlReader->getAttribute('id');
                    $offer['available'] = $this->xmlReader->getAttribute('available');

                    foreach ($dom->childNodes as $node) {
                        if ($node->nodeType == XML_ELEMENT_NODE) {
                            if ($node->nodeName == self::PICTURE_ELEMENT_NAME)
                                $offer[$node->nodeName][] = $node->nodeValue;
                            else
                                $offer[$node->nodeName] = $node->nodeValue;
                        }
                    }

                    $this->offers[] = $offer;
                }

                if ($this->xmlReader->localName == self::COMPANY_ELEMENT_NAME) {
                    if ($this->xmlReader->nodeType == XMLReader::TEXT) {
                        $this->company = $this->xmlReader->value;
                    }
                }

                if ($this->xmlReader->localName == self::URL_ELEMENT_NAME) {
                    if ($this->xmlReader->nodeType == XMLReader::TEXT) {
                        $this->url = $this->xmlReader->value;
                    }
                }

                if ($this->xmlReader->localName == self::DATE_ELEMENT_NAME) {
                    $this->date = $this->xmlReader->getAttribute('date');
                }

                if ($this->xmlReader->localName == self::CURRENCY_ELEMENT_NAME) {
                    $this->currency = $this->xmlReader->getAttribute('id');
                }

                if ($this->xmlReader->localName == self::CATEGORY_ELEMENT_NAME) {
                    $category = [];
                    $category['id'] = $this->xmlReader->getAttribute('id');
                    $category['name'] = $this->xmlReader->readString();

                    if ($this->xmlReader->nodeType == XMLReader::TEXT) {
                        $category['name'] = $this->xmlReader->value;
                    }
                    $this->categories[] = $category;
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return array
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }
}