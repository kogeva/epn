<?php

require_once "ePNDump.php";

const DIP_LINK = 'o32nmncbvj7poh6jvu8aroqvl76z9acq';

function xml2assoc($xml, $name)
{
    $tree = null;

    while($xml->read())
    {
        if($xml->nodeType == XMLReader::END_ELEMENT)
        {
            return $tree;
        }

        else if($xml->nodeType == XMLReader::ELEMENT)
        {
            $node = array();

            $node['tag'] = $xml->name;

            if($xml->hasAttributes)
            {
                $attributes = array();
                while($xml->moveToNextAttribute())
                {
                    $attributes[$xml->name] = $xml->value;
                }
                $node['attr'] = $attributes;
            }

            if(!$xml->isEmptyElement)
            {
                $childs = xml2assoc($xml, $node['tag']);
                $node['childs'] = $childs;
            }
            $tree[] = $node;
        }

        else if($xml->nodeType == XMLReader::TEXT)
        {
            $node = array();
            $node['text'] = $xml->value;
            $tree[] = $node;
        }
    }
   return $tree;
}

$xml = new XMLReader();
$xml->open('/media/data/door/ePN/ePNDump/alidump_large.yml');
$assoc = xml2assoc($xml, "root");
$xml->close();

print_r($assoc);
echo "</PRE>";
